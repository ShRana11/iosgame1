//
//  GameScene.swift
//  FirstGame
//
//  Created by Sukhwinder Rana on 2019-06-05.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    override func didMove(to view: SKView) {
        //get rid of everything
        //add some text to screen
        let score = SKLabelNode(text: "hello world");
        score.position = CGPoint(x: 100,y: 100)
        score.fontName = "Avenir"
        score.fontSize = 40
        score.fontColor = SKColor.yellow
        addChild(score)
        
        
        let square = SKSpriteNode(color: SKColor.red, size: CGSize(width: 100, height: 100))
        square.position = CGPoint(x: 300, y: 300)
        addChild(square)
        
        let image = SKSpriteNode(imageNamed: "pokemon")
        image.position = CGPoint(x: 200, y: 200)
        image.size.height = 80
        image.size.width = 80
        addChild(image)
        
        let move = SKAction.moveBy(x: 100, y: 0, duration: 3)
         let move1 = SKAction.moveBy(x: 0, y: 100, duration: 3)
         let move2 = SKAction.moveBy(x: -100, y: 0, duration: 3)
         let move3 = SKAction.moveBy(x: 0, y: -100, duration: 3)
        let sequence: SKAction = SKAction.sequence([move, move1, move2, move3])
        image.run(sequence)
        //image.run(move)
        
        
        let height = self.frame.size.height
        let width = self.frame.size.width
        
        
       
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let loc = touch.location(in: self)
            let nod = atPoint(loc)
            if (loc.y < self.size.height/2){
                print("down")
            }
            if(loc.y >= self.size.height/2){
                print("up")
                
            }
        }
    }
}

