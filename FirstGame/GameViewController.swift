//
//  GameViewController.swift
//  FirstGame
//
//  Created by Sukhwinder Rana on 2019-06-05.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = GameScene(size: view.frame.size)
        let skView = view as! SKView
        
      
        skView.showsFPS = true;
        skView.showsNodeCount =  false;
        
        skView.presentScene(scene);
        
    
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
